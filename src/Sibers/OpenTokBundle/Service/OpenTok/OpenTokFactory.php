<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 13.08.15
 * Time: 16:03
 */

namespace Sibers\OpenTokBundle\Service\OpenTok;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\Session;
use OpenTok\Role;

class OpenTokFactory
{
    /**
     * @var Container $container
     */
    protected $container;

    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $apiSecret;

    /**
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;

        $this->apiKey = $this->container->getParameter('tokbox_api_key');
        $this->apiSecret = $this->container->getParameter('tokbox_api_secret');
    }

    /**
     * @param array $sessionOptions
     * @param array $tokenOptions
     * @return null|OpenTokWrapper
     */
    public function getNewInstance($sessionOptions = array(), $tokenOptions = array())
    {

        try {
            $opentok = new OpenTok($this->apiKey, $this->apiSecret);

            $session = $opentok->createSession($sessionOptions);

            $sessionId = $session->getSessionId();

            $token = $opentok->generateToken($sessionId, $tokenOptions);

            return new OpenTokWrapper($this->apiKey, $this->apiSecret, $opentok, $sessionId, $token);

        } catch (\Exception $ex) {

            return array(
                'error_code' => $ex->getCode(),
                'message' => $ex->getMessage()
            );
        }

        return false;
    }

    /**
     * @param string $sessionId
     * @param array $tokenOptions
     * @return null|OpenTokWrapper
     */
    public function getInstanceForSessionId($sessionId, $tokenOptions = array())
    {

        try {
            $opentok = new OpenTok($this->apiKey, $this->apiSecret);

            $token = $opentok->generateToken($sessionId, $tokenOptions);

            return new OpenTokWrapper($this->apiKey, $this->apiSecret, $opentok, $sessionId, $token);

        } catch (\Exception $ex) {

            return array(
                'error_code' => $ex->getCode(),
                'message' => $ex->getMessage()
            );
        }

        return false;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }
}
