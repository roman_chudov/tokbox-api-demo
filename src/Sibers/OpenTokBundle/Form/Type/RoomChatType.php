<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 17.08.15
 * Time: 15:23
 */

namespace Sibers\OpenTokBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RoomChatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('otSessionId', 'entity', array(
                'class' => 'SibersOpenTokBundle:RoomChat',
                'query_builder' => function (EntityRepository $er) {

                    $qb = $er->createQueryBuilder('s');

                    return $qb;
                },
            ));
    }

    public function getName()
    {
        return 'room_chat';
    }
}
