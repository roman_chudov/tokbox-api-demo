<?php

namespace Sibers\OpenTokBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersChatMessage
 *
 * @ORM\Entity(repositoryClass="Sibers\OpenTokBundle\Entity\UsersChatMessageRepository")
 */
class UsersChatMessage extends Message
{
    /**
     * @var UsersChat
     *
     * @ORM\JoinColumn(name="usersChat", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Sibers\OpenTokBundle\Entity\UsersChat")
     */
    protected $usersChat;

    /**
     * @return UsersChat
     */
    public function getUsersChat()
    {
        return $this->usersChat;
    }

    /**
     * @param UsersChat $usersChat
     */
    public function setUsersChat($usersChat)
    {
        $this->usersChat = $usersChat;
    }

    public function getType()
    {
        return 'users_chat';
    }

    public function getEntityName()
    {
        return 'UsersChatMessage';
    }
}
