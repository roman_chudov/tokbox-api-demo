<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10.09.15
 * Time: 17:27
 */

namespace Sibers\OpenTokBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Util\StringUtils;

/**
 * Class MobileAuthController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 *
 * @Route("/mobile")
 */
class MobileAuthController extends Controller
{
    /**
     * @Route("/login", name="mobile_login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        if (!$username || !$password) {
            return new JsonResponse(array(
                'success' => false,
                'error_code' => $this->getParameter('username_password_not_set_error_code'),
                'message' => $this->getParameter('username_password_not_set_message')
            ));
        }

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsernameOrEmail($username);

        if ($user) {

            $encoder = new MessageDigestPasswordEncoder();
            $encodedPwd = $encoder->encodePassword($password, $user->getSalt());

            if (StringUtils::equals($user->getPassword(), $encodedPwd)) {

                $tokensManager = $this->get('mobile_access_token.orm.manager');

                $tokensCreateLimitAchieved = false;

                $tokensTodayCreatedCnt = $tokensManager->getTokensCreatedTodayCnt($user);

                if ($tokensTodayCreatedCnt >= $this->getParameter('max_tokens_per_day')) {

                    $tokensCreateLimitAchieved = true;
                    $access_token = $tokensManager->getLastActive($user);
                } else {

                    $access_token = $tokensManager->createNewAccessToken($user);
                }

                if ($access_token) {
                    return new JsonResponse(array(
                        'success' => true,
                        'access_token' => $access_token->getAccessToken(),
                        'expires_at' => gmdate('m-d-Y H:i:s', $access_token->getExpiresAt()->getTimestamp()),
                        'is_new_generated_token' => (!$tokensCreateLimitAchieved)
                    ));
                } else {
                    // TODO: it's will be removed
                    return new JsonResponse(array(
                        'success' => false,
                        'error_code' => $this->getParameter('tokens_create_limit_achieved_error_code'),
                        'message' => $this->getParameter('tokens_create_limit_achieved_message')
                    ));
                }
            } else {

                return new JsonResponse(array(
                    'success' => false,
                    'error_code' => $this->getParameter('password_wrong_error_code'),
                    'message' => $this->getParameter('password_wrong_message')
                ));
            }
        } else {

            return new JsonResponse(array(
                'success' => false,
                'error_code' => $this->getParameter('username_not_found_error_code'),
                'message' => $this->getParameter('username_not_found_message')
            ));
        }
    }

    /**
     * @Route("/login/check", name="mobile_login_check")
     * @Template()
     */
    public function loginCheckAction(Request $request)
    {
        return new JsonResponse(array(
            'success' => true
        ));
    }
}