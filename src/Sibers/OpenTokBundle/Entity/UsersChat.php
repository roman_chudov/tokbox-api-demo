<?php

namespace Sibers\OpenTokBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersChat
 *
 * @ORM\Entity(repositoryClass="Sibers\OpenTokBundle\Entity\UsersChatRepository")
 */
class UsersChat extends Session
{
    /**
     * @var User
     *
     * @ORM\JoinColumn(name="user1", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Sibers\OpenTokBundle\Entity\User")
     */
    protected $user1;

    /**
     * @var User
     *
     * @ORM\JoinColumn(name="user2", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Sibers\OpenTokBundle\Entity\User")
     */
    protected $user2;


    /**
     * @return User
     */
    public function getUser1()
    {
        return $this->user1;
    }

    /**
     * @param User $user1
     */
    public function setUser1($user1)
    {
        $this->user1 = $user1;
    }

    /**
     * @return User
     */
    public function getUser2()
    {
        return $this->user2;
    }

    /**
     * @param User $user2
     */
    public function setUser2($user2)
    {
        $this->user2 = $user2;
    }

    public function getType()
    {
        return 'users_chat';
    }

    public function getEntityName()
    {
        return 'UsersChat';
    }
}
