<?php

namespace Sibers\OpenTokBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoomChatMessage
 *
 * @ORM\Entity(repositoryClass="Sibers\OpenTokBundle\Entity\RoomChatMessageRepository")
 */
class RoomChatMessage extends Message
{

    /**
     * @var RoomChat
     *
     * @ORM\JoinColumn(name="roomChat", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="Sibers\OpenTokBundle\Entity\RoomChat")
     */
    protected $roomChat;

    /**
     * @return RoomChat
     */
    public function getRoomChat()
    {
        return $this->roomChat;
    }

    /**
     * @param RoomChat $roomChat
     */
    public function setRoomChat($roomChat)
    {
        $this->roomChat = $roomChat;
    }

    public function getType()
    {
        return 'room_chat';
    }

    public function getEntityName()
    {
        return 'RoomChatMessage';
    }
}
