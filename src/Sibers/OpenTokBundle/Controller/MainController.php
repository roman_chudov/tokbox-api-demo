<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 13.08.15
 * Time: 14:07
 */

namespace Sibers\OpenTokBundle\Controller;

use Sibers\OpenTokBundle\Entity\RoomChat;
use Sibers\OpenTokBundle\Entity\User;

use Sibers\OpenTokBundle\Form\Type\RoomChatType;
use Sibers\OpenTokBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class MainController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 */
class MainController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $room = new RoomChat();

        $form = $this->createForm(new RoomChatType(), $room);

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/userschat", name="userschat")
     * @Template()
     */
    public function usersChatAction()
    {
        $user = new User();

        $form = $this->createForm(new UserType($this->getUser()->getId()), $user);

        return array(
            'form' => $form->createView()
        );
    }
}