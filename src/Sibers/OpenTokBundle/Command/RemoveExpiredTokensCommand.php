<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 23.09.15
 * Time: 15:20
 */

namespace Sibers\OpenTokBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveExpiredTokensCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('sibers:remove_expired_tokens')
            ->setDescription('Remove expired mobile access tokens')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $tokens = $em->getRepository('SibersOpenTokBundle:MobileAccessToken')->getExpiredAccessTokens();
        if($tokens){
            foreach($tokens as $token){
                $em->remove($token);
            }

            $em->flush();

            $output->writeln(count($tokens) . ' token(s) removed');
        }
    }
}