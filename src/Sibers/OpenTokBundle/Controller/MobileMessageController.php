<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.09.15
 * Time: 12:48
 */

namespace Sibers\OpenTokBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class MobileMessageController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 *
 * @Route("/api/mobile/message")
 */
class MobileMessageController extends Controller
{
    /**
     * @Route("/get/for/room/{id}", name="get_messages_for_room_mobile")
     * @Template()
     */
    public function getMessagesForRoomAction($id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $page = $request->get('page', 1);
        $messages_cnt_per_page = $request->get('per_page', $this->getParameter('messages_count_per_page'));

        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->getMessagesForRoom(
                $this->getUser(),
                $id,
                $page,
                $messages_cnt_per_page
            )
        );
    }

    /**
     * @Route("/get/for/userschat/{id}", name="get_messages_for_users_chat_mobile")
     * @Template()
     */
    public function getMessagesForUsersChatAction($id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $page = $request->get('page', 1);
        $messages_cnt_per_page = $request->get('per_page', $this->getParameter('messages_count_per_page'));

        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->getMessagesForUsersChat(
                $this->getUser(),
                $id,
                $page,
                $messages_cnt_per_page
            )
        );
    }

    /**
     * @Route("/put/room/{room_id}", name="put_new_message_for_room_mobile")
     * @Template()
     */
    public function putNewRoomMessageAction($room_id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->putNewRoomMessage(
                $this->getUser(),
                $room_id,
                $request->get('message_body')
            )
        );
    }

    /**
     * @Route("/put/userschat/{chat_id}", name="put_new_message_for_users_chat_mobile")
     * @Template()
     */
    public function putNewUsersChatMessageAction($chat_id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->putNewUsersChatMessage(
                $this->getUser(),
                $chat_id,
                $request->get('message_body')
            )
        );
    }
}