<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 13.08.15
 * Time: 17:28
 */

namespace Sibers\OpenTokBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    private $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('username', 'entity', array(
                'class' => 'SibersOpenTokBundle:User',
                'query_builder' => function (EntityRepository $er) {

                    $qb = $er->createQueryBuilder('u');
                    $qb->where($qb->expr()->neq('u.id', $this->user_id));

                    return $qb;
                },
            ));
    }

//    public function setDefaultOptions(OptionsResolver $resolver)
//    {
//        $resolver->setDefaults(array(
//            'data_class' => 'Sibers\OpenTokBundle\Entity\User'
//        ));
//    }

    public function getName()
    {
        return 'user';
    }
}
