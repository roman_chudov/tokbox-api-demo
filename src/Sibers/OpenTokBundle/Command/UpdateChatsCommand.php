<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 21.09.15
 * Time: 17:27
 */

namespace Sibers\OpenTokBundle\Command;

use Sibers\OpenTokBundle\Entity\Session;
use Sibers\OpenTokBundle\Entity\UsersChat;
use Sibers\OpenTokBundle\Service\OpenTok\OpenTokWrapper;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateChatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('sibers:update_chats')
            ->setDescription('Update OpenTok Session ID for existing chats if API Key changed')
//            ->addArgument(
//                'name',
//                InputArgument::OPTIONAL,
//                'Who do you want to greet?'
//            )
//            ->addOption(
//                'yell',
//                null,
//                InputOption::VALUE_NONE,
//                'If set, the task will yell in uppercase letters'
//            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $success = true;
        $error_messages = array();

        $chats = $em->getRepository('SibersOpenTokBundle:Session')->findAll();

        /** @var Session $chat */
        foreach ($chats as $chat) {

            if ($chat->getType() == 'room_chat') {
                $data = 'Room Chat';
            } else if ($chat->getType() == 'users_chat') {
                /** @var UsersChat $chat */
                $data = $chat->getUser1()->getUsername() . ' & ' . $chat->getUser2()->getUsername();
            } else {
                $data = 'Chat';
            }

            $opentok = $this->getContainer()->get('sibers_open_tok.opentok_factory')->getNewInstance(
                array(),
                array(
                    'data' => $data
                )
            );

            if ($opentok instanceof OpenTokWrapper) {

                $chat->setOtSessionId($opentok->getSessionId());
                $em->persist($chat);

            } else if (is_array($opentok)) {

                $success = false;
                array_push($error_messages, $chat->getType() . ' [' . $chat->getId() . ']: ' . $opentok['message']);
            }
        }

        $em->flush();

        $output->writeln('Success: ' . ($success ? 'true' : 'false'));
        if (count($error_messages)) {
            $output->writeln('Errors:');
            foreach ($error_messages as $err_msg) {
                $output->writeln($err_msg);
            }
        }
    }
}