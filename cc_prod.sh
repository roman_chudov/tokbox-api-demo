#!/usr/bin/env bash
sudo chmod -R 777 app/cache/ app/logs/
php app/console cache:clear --env=prod
sudo chmod -R 777 app/cache/ app/logs/