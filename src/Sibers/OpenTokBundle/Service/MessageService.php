<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.09.15
 * Time: 12:54
 */

namespace Sibers\OpenTokBundle\Service;

use Doctrine\ORM\EntityManager;
use Sibers\OpenTokBundle\Entity\RoomChatMessage;
use Sibers\OpenTokBundle\Entity\User;
use Sibers\OpenTokBundle\Entity\UsersChatMessage;
use Symfony\Component\DependencyInjection\Container;

class MessageService
{
    /**
     * @var Container $container
     */
    protected $container;

    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param User $me
     * @param $room_id
     * @param int $page
     * @param int $messages_cnt_per_page
     * @return array
     */
    public function getMessagesForRoom(User $me, $room_id, $page = 1, $messages_cnt_per_page = 10)
    {
        $room = $this->em->getRepository('SibersOpenTokBundle:RoomChat')->find($room_id);

        if ($room) {

            $repo = $this->em->getRepository('SibersOpenTokBundle:RoomChatMessage');

            $messages = $repo->getMessagesForChat($room_id, $page, $messages_cnt_per_page);
            $total = $repo->getTotal($room_id);

            $result = array();

            if ($messages) {

                /** @var RoomChatMessage $msg */
                foreach ($messages as $msg) {
                    $result[] = array(
                        'from' => $msg->getUserFrom()->getUsername(),
                        'body' => $msg->getBody(),
                        'datetime' => $msg->getCreated()
                    );
                }
            }

            return array(
                'success' => true,
                'count' => count($result),
                'page' => $page,
                'per_page' => $messages_cnt_per_page,
                'total_messages' => $total,
                'total_pages' => $this->getTotalPages($total, $messages_cnt_per_page),
                'messages' => $result
            );
        }

        return array(
            'success' => false,
            'error_code' => $this->container->getParameter('chat_not_found_error_code'),
            'message' => $this->container->getParameter('chat_not_found_message')
        );
    }

    /**
     * @param User $me
     * @param $userschat_id
     * @param int $page
     * @param int $messages_cnt_per_page
     * @return array
     */
    public function getMessagesForUsersChat(User $me, $userschat_id, $page = 1, $messages_cnt_per_page = 10)
    {
        $chat = $this->em->getRepository('SibersOpenTokBundle:UsersChat')->find($userschat_id);

        if ($chat) {

            $repo = $this->em->getRepository('SibersOpenTokBundle:UsersChatMessage');

            $messages = $repo->getMessagesForChat($userschat_id, $page, $messages_cnt_per_page);
            $total = $repo->getTotal($userschat_id);

            $result = array();

            if ($messages) {

                /** @var UsersChatMessage $msg */
                foreach ($messages as $msg) {
                    $result[] = array(
                        'from' => $msg->getUserFrom()->getUsername(),
                        'body' => $msg->getBody(),
                        'datetime' => $msg->getCreated()
                    );
                }
            }

            return array(
                'success' => true,
                'count' => count($result),
                'page' => $page,
                'per_page' => $messages_cnt_per_page,
                'total_messages' => $total,
                'total_pages' => $this->getTotalPages($total, $messages_cnt_per_page),
                'messages' => $result
            );
        }

        return array(
            'success' => false,
            'error_code' => $this->container->getParameter('chat_not_found_error_code'),
            'message' => $this->container->getParameter('chat_not_found_message')
        );
    }

    /**
     * @param User $me
     * @param $room_id
     * @param $message_body
     * @return array
     */
    public function putNewRoomMessage(User $me, $room_id, $message_body)
    {

        if ($message_body) {
            $room = $this->em->getRepository('SibersOpenTokBundle:RoomChat')->find($room_id);
            if ($room) {

                $message = new RoomChatMessage();
                $message->setBody($message_body);
                $message->setRoomChat($room);
                $message->setUserFrom($me);

                $this->em->persist($message);
                $this->em->flush();

                return array('success' => true);
            } else {
                $error_code = $this->container->getParameter('chat_not_found_error_code');
                $error_msg = $this->container->getParameter('chat_not_found_message');
            }
        } else {
            $error_code = $this->container->getParameter('message_body_is_null_error_code');
            $error_msg = $this->container->getParameter('message_body_is_null_message');
        }

        return array(
            'success' => false,
            'error_code' => $error_code,
            'message' => $error_msg
        );
    }

    /**
     * @param User $me
     * @param $chat_id
     * @param $message_body
     * @return array
     */
    public function putNewUsersChatMessage(User $me, $chat_id, $message_body)
    {
        if ($message_body) {
            $chat = $this->em->getRepository('SibersOpenTokBundle:UsersChat')->find($chat_id);
            if ($chat) {

                $message = new UsersChatMessage();
                $message->setBody($message_body);
                $message->setUsersChat($chat);
                $message->setUserFrom($me);

                $this->em->persist($message);
                $this->em->flush();

                return array('success' => true);
            } else {
                $error_code = $this->container->getParameter('chat_not_found_error_code');
                $error_msg = $this->container->getParameter('chat_not_found_message');
            }
        } else {
            $error_code = $this->container->getParameter('message_body_is_null_error_code');
            $error_msg = $this->container->getParameter('message_body_is_null_message');
        }

        return array(
            'success' => false,
            'error_code' => $error_code,
            'message' => $error_msg
        );
    }

    protected function getTotalPages($total_messages, $per_page)
    {
        return intval($total_messages / $per_page) + ((intval($total_messages % $per_page)) > 0 ? 1 : 0);
    }
}