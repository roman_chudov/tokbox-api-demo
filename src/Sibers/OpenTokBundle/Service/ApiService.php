<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.09.15
 * Time: 12:54
 */

namespace Sibers\OpenTokBundle\Service;

use Doctrine\ORM\EntityManager;
use Sibers\OpenTokBundle\Entity\RoomChat;
use Sibers\OpenTokBundle\Entity\User;
use Sibers\OpenTokBundle\Entity\UsersChat;
use Sibers\OpenTokBundle\Service\OpenTok\OpenTokWrapper;
use Symfony\Component\DependencyInjection\Container;

class ApiService
{
    /**
     * @var Container $container
     */
    protected $container;

    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param User $me
     * @return array
     */
    public function createNewRoom(User $me)
    {
        $opentok = $this->container->get('sibers_open_tok.opentok_factory')->getNewInstance(
            array(),
            array(
                'data' => 'Chat Room'
            )
        );

        if ($opentok instanceof OpenTokWrapper) {

            $room = new RoomChat();
            $room->setOtSessionId($opentok->getSessionId());

            $this->em->persist($room);
            $this->em->flush();

            return array(
                'success' => true,
                'internal_session_id' => $room->getId()
            );
        } else if (is_array($opentok)) {

            return array_merge(array('success' => false), $opentok);
        }

        return array('success' => false);
    }

    /**
     * @param User $me
     * @param $user2_id
     * @return array
     */
    public function createUsersChatForUser2(User $me, $user2_id)
    {
        if ($me->getId() == intval($user2_id)) {

            return array(
                'success' => false,
                'error_code' => $this->container->getParameter('user1_user2_eq_error_code'),
                'message' => $this->container->getParameter('user1_user2_eq_message')
            );
        } else {

            $user2 = $this->em->getRepository('SibersOpenTokBundle:User')->find($user2_id);

            if ($user2) {

                $usersChat = $this->em->getRepository('SibersOpenTokBundle:UsersChat')->getByUsers($me, $user2);
                if ($usersChat) {

//                    return array(
//                        'success' => false,
//                        'error_code' => $this->container->getParameter('userschat_already_exist_error_code'),
//                        'message' => $this->container->getParameter('userschat_already_exist_message')
//                    );
                    return array(
                        'success' => true,
                        'internal_session_id' => $usersChat->getId()
                    );
                } else {

                    $opentok = $this->container->get('sibers_open_tok.opentok_factory')->getNewInstance(
                        array(),
                        array(
                            'data' => $me->getUsername() . ' & ' . $user2->getUsername()
                        )
                    );

                    if ($opentok instanceof OpenTokWrapper) {

                        $usersChat = new UsersChat();
                        $usersChat->setOtSessionId($opentok->getSessionId());
                        $usersChat->setUser1($me);
                        $usersChat->setUser2($user2);

                        $this->em->persist($usersChat);
                        $this->em->flush();

                        return array(
                            'success' => true,
                            'internal_session_id' => $usersChat->getId()
                        );
                    } else if (is_array($opentok)) {

                        return array_merge(array('success' => false), $opentok);
                    }
                }
            }

            return array(
                'success' => false,
                'error_code' => $this->container->getParameter('user2_not_found_error_code'),
                'message' => $this->container->getParameter('user2_not_found_message')
            );
        }
    }

    /**
     * @param User $me
     * @param $internal_session_id
     * @return array
     */
    public function getSessionParams(User $me, $internal_session_id)
    {
        $internal_session = $this->em->getRepository('SibersOpenTokBundle:Session')->find($internal_session_id);

        if ($internal_session) {

            $opentok = $this->container->get('sibers_open_tok.opentok_factory')->getInstanceForSessionId(
                $internal_session->getOtSessionId(),
                array(
                    'data' => $me->getUsername()
                )
            );

            if ($opentok instanceof OpenTokWrapper) {

                return array(
                    'success' => true,
                    'api_key' => $opentok->getApiKey(),
                    'session_id' => $internal_session->getOtSessionId(),
                    'token' => $opentok->getToken()
                );
            } else if (is_array($opentok)) {

                return array_merge(array('success' => false), $opentok);
            }
        }

        return array(
            'success' => false,
            'error_code' => $this->container->getParameter('chat_not_found_error_code'),
            'message' => $this->container->getParameter('chat_not_found_message')
        );
    }

    /**
     * @param User $me
     * @param $user2_id
     * @return array
     */
    public function getSessionByUser2(User $me, $user2_id)
    {
        $user2 = $this->em->getRepository('SibersOpenTokBundle:User')->find($user2_id);

        if ($user2) {

            $usersChat = $this->em->getRepository('SibersOpenTokBundle:UsersChat')->getByUsers($me, $user2);

            if ($usersChat) {

                $opentok = $this->container->get('sibers_open_tok.opentok_factory')->getInstanceForSessionId(
                    $usersChat->getOtSessionId(),
                    array(
                        'data' => $me->getUsername()
                    )
                );

                if ($opentok instanceof OpenTokWrapper) {
                    return array(
                        'success' => true,
                        'api_key' => $this->container->getParameter('tokbox_api_key'),
                        'session_id' => $usersChat->getOtSessionId(),
                        'token' => $opentok->getToken(),
                        'internal_session_id' => $usersChat->getId()
                    );
                } else if (is_array($opentok)) {

                    return array_merge(array('success' => false), $opentok);
                }
            }
        }

        return array(
            'success' => false,
            'error_code' => $this->container->getParameter('user2_not_found_error_code'),
            'message' => $this->container->getParameter('user2_not_found_message')
        );
    }
}