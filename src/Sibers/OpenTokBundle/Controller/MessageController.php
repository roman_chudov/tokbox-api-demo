<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 17.08.15
 * Time: 17:52
 */

namespace Sibers\OpenTokBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MessageController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 *
 * @Route("/message")
 */
class MessageController extends Controller
{

    /**
     * @Route("/get/for/room/{id}", name="get_messages_for_room")
     * @Template()
     */
    public function getMessagesForRoomAction($id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $page = $request->get('page', 1);
        $messages_cnt_per_page = $request->get('per_page', $this->getParameter('messages_count_per_page'));

        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->getMessagesForRoom(
                $this->getUser(),
                $id,
                $page,
                $messages_cnt_per_page
            )
        );
    }

    /**
     * @Route("/get/for/userschat/{id}", name="get_messages_for_users_chat")
     * @Template()
     */
    public function getMessagesForUsersChatAction($id)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $page = $request->get('page', 1);
        $messages_cnt_per_page = $request->get('per_page', $this->getParameter('messages_count_per_page'));

        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->getMessagesForUsersChat(
                $this->getUser(),
                $id,
                $page,
                $messages_cnt_per_page
            )
        );
    }

    /**
     * @Route("/put/room/{room_id}/body/{body}", name="put_new_message_for_room")
     * @Template()
     */
    public function putNewRoomMessageAction($room_id, $body)
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->putNewRoomMessage(
                $this->getUser(),
                $room_id,
                $body
            )
        );
    }

    /**
     * @Route("/put/userschat/{chat_id}/body/{body}", name="put_new_message_for_users_chat")
     * @Template()
     */
    public function putNewUsersChatMessageAction($chat_id, $body)
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.message_service')->putNewUsersChatMessage(
                $this->getUser(),
                $chat_id,
                $body
            )
        );
    }
}