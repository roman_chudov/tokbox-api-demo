/**
 * Created by chudov on 17.08.15.
 */

var ISID = 0;

$(document).ready(function () {
    $("#usermsg").attr('disabled', 'disabled');
    $('#user_username').addClass('form-control');

    initStartChatBtn();
    initStopChatBtn();
    initSendBtn();
});

function getSessionParamsAndInitSession(internal_session_id) {
    ISID = internal_session_id;

    $.get(SERVER_BASE_URL + '/api/protected/get/sessionparams/' + internal_session_id, function (res) {

        if (res.success) {

            apiKey = res.api_key;
            sessionId = res.session_id;
            token = res.token;

            console.log('API Key: ' + apiKey);
            console.log('Session ID: ' + sessionId);
            console.log('Token: ' + token);

            getOldMessages(SERVER_BASE_URL + '/message/get/for/userschat/' + ISID);
            initializeSession();
        } else {
            alert('error');
            $('#start_chat').removeAttr('disabled');
        }
    });
}

function initStartChatBtn() {
    $('#start_chat').click(function () {
        $('#chatbox').html('');
        $('#start_chat').attr('disabled', 'disabled');
        $('#user_username').attr('disabled', 'disabled');

        var user2_id = $('#user_username').val();
        if (user2_id) {

            $.get(SERVER_BASE_URL + '/api/protected/get/session/by/user2/' + user2_id, function (res) {

                if (res.success) {

                    console.log('Internal SID:' + res.internal_session_id);
                    getSessionParamsAndInitSession(res.internal_session_id);
                } else {

                    $.get(SERVER_BASE_URL + '/api/protected/create/userschat/for/user2/' + user2_id, function (create_res) {

                        if (create_res.success) {

                            console.log('Internal SID:' + create_res.internal_session_id);
                            getSessionParamsAndInitSession(create_res.internal_session_id);
                        }
                    });
                }
            });

        } else {
            alert('Select user!');
            $('#start_chat').removeAttr('disabled');
            $('#user_username').removeAttr('disabled');
        }
    });
}

function initStopChatBtn() {
    $('#stop_chat').hide();

    $('#stop_chat').click(function () {
        if (session) {
            session.disconnect();
        } else {
            alert('Session not created!');
        }
    });
}

function initSendBtn() {

    $("#submitmsg").attr('disabled', 'disabled');

    $("#submitmsg").click(function () {
        var clientmsg = $("#usermsg").val().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        $("#usermsg").val('');

        $.get(SERVER_BASE_URL + '/message/put/userschat/' + ISID + '/body/' + clientmsg, function (res) {
        });

        session.signal(
            {
                data: clientmsg,
                type: 'all'
            },
            function (error) {
                if (error) {
                    console.log("signal error ("
                    + error.code
                    + "): " + error.message);
                } else {
                    console.log("signal sent.");
                }
            }
        );
    });
}
