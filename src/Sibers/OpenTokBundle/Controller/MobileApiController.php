<?php

namespace Sibers\OpenTokBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class MobileApiController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 *
 * @Route("/api/mobile")
 */
class MobileApiController extends Controller
{
    /**
     * @Route("/create/room", name="create_new_room_mobile")
     * @Template()
     */
    public function createNewRoomAction()
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.api_service')->createNewRoom($this->getUser())
        );
    }

    /**
     * @Route("/create/userschat/for/user2/{id}", name="create_userschat_for_user2_mobile")
     * @Template()
     */
    public function createUsersChatForUser2Action($id)
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.api_service')->createUsersChatForUser2($this->getUser(), $id)
        );
    }

    /**
     * @Route("/get/sessionparams/{internal_session_id}", name="get_session_params_mobile")
     * @Template()
     */
    public function getSessionParamsAction($internal_session_id)
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.api_service')->getSessionParams($this->getUser(), $internal_session_id)
        );
    }

    /**
     * @Route("/get/session/by/user2/{id}", name="get_session_by_user2_mobile")
     * @Template()
     */
    public function getSessionByUser2Action($id)
    {
        return new JsonResponse(
            $this->get('sibers_open_tok.api_service')->getSessionByUser2($this->getUser(), $id)
        );
    }
}
