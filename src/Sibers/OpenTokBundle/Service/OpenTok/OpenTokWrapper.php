<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 13.08.15
 * Time: 16:13
 */

namespace Sibers\OpenTokBundle\Service\OpenTok;

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\Session;
use OpenTok\Role;

class OpenTokWrapper
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $apiSecret;

    /**
     * @var OpenTok
     */
    protected $opentok;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * @var string
     */
    protected $token;

    /**
     * OpenTokWrapper constructor.
     * @param string $apiKey
     * @param string $apiSecret
     * @param OpenTok $opentok
     * @param string $sessionId
     * @param string $token
     */
    public function __construct($apiKey, $apiSecret, OpenTok $opentok, $sessionId, $token)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->opentok = $opentok;
        $this->sessionId = $sessionId;
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param string $apiSecret
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;
    }

    /**
     * @return OpenTok
     */
    public function getOpentok()
    {
        return $this->opentok;
    }

    /**
     * @param OpenTok $opentok
     */
    public function setOpentok($opentok)
    {
        $this->opentok = $opentok;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }


}