<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 20.08.15
 * Time: 14:23
 */

namespace Sibers\OpenTokBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class WsseUserToken extends AbstractToken
{
//    public $created;
//    public $digest;
//    public $nonce;
    public $access_token;

    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    public function getCredentials()
    {
        return '';
    }
}