<?php

namespace Sibers\OpenTokBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="Sibers\OpenTokBundle\Entity\SessionRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"room_chat" = "RoomChat", "users_chat" = "UsersChat"})
 */
abstract class Session
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $otSessionId
     *
     * @ORM\Column(name="ot_session_id", type="text")
     */
    protected $otSessionId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOtSessionId()
    {
        return $this->otSessionId;
    }

    /**
     * @param string $otSessionId
     */
    public function setOtSessionId($otSessionId)
    {
        $this->otSessionId = $otSessionId;
    }

    abstract public function getType();

    abstract public function getEntityName();
}
