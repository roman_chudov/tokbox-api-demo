<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 20.08.15
 * Time: 14:25
 */

namespace Sibers\OpenTokBundle\Security\Firewall;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

use Sibers\OpenTokBundle\Security\Authentication\Token\WsseUserToken;

class WsseListener implements ListenerInterface
{
    protected $tokenStorage;
    protected $authenticationManager;
    protected $parameterBag;

    public function __construct(TokenStorageInterface $tokenStorage,
                                AuthenticationManagerInterface $authenticationManager,
                                ParameterBagInterface $parameterBag
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->parameterBag = $parameterBag;
    }

    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        try {
            $wsseRegex = '/UsernameToken Username="([^"]+)", AccessToken="(.+)"/';

            if (!$request->headers->has('x-wsse') ||
                1 !== preg_match($wsseRegex, $request->headers->get('x-wsse'), $matches)
            ) {
                throw new AuthenticationException($this->parameterBag->get('mobile_wsse_header_wrong_message'),
                    $this->parameterBag->get('mobile_wsse_header_wrong_error_code')
                );
            }

            $token = new WsseUserToken();
            $token->setUser($matches[1]);
            $token->access_token = $matches[2];

            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);

            return;
        } catch (AuthenticationException $ex) {
            // ... you might log something here

            // To deny the authentication clear the token. This will redirect to the login page.
            // Make sure to only clear your token, not those of other authentication listeners.
            // $token = $this->tokenStorage->getToken();
            // if ($token instanceof WsseUserToken && $this->providerKey === $token->getProviderKey()) {
            //     $this->tokenStorage->setToken(null);
            // }
            // return;
        }

        // By default deny authorization
        $response = new JsonResponse(array(
            'success' => false,
            'error_code' => $ex->getCode(),
            'message' => $ex->getMessage()
        ));
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }
}