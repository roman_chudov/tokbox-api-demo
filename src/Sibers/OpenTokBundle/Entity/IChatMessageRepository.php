<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 22.09.15
 * Time: 19:45
 */

namespace Sibers\OpenTokBundle\Entity;


interface IChatMessageRepository
{

    public function getMessagesForChat($chat_id, $page = 1, $messages_cnt_per_page = 10);

    public function getTotal($chat_id);
}