/**
 * Created by chudov on 14.08.15.
 */

function initializeSession() {

    connectionCount = 0;
    current_to_connection_id = null;

    // Initialize Session Object
    session = OT.initSession(apiKey, sessionId);

    // Handler for connectionCreated event
    session.on("connectionCreated", function (event) {
        ++connectionCount;
        if (event.connection.connectionId != session.connection.connectionId) {
            console.log('Another client connected. ' + connectionCount + ' total.');
        } else {
            console.log('Your client connected. ' + connectionCount + ' total.');
        }

        initUserListItem(event.connection);
    });

    // Handler for connectionDestroyed event
    session.on("connectionDestroyed", function (event) {
        --connectionCount;
        console.log('A client ' + event.connection.data + ' disconnected. ' + connectionCount + ' total.');
        $('#' + event.connection.connectionId).remove();
        $('#p2p-' + event.connection.connectionId).remove();
        $('#welcome').html('');

        if (current_to_connection_id == event.connection.connectionId) {
            current_to_connection_id = null;
        }
    });

    // Handler for sessionDisconnected event
    session.on('sessionDisconnected', function (event) {
        $('#start_chat').show();
        $('#stop_chat').hide();
        $("#submitmsg").attr('disabled', 'disabled');
        $("#usermsg").attr('disabled', 'disabled');
        $("#create_new_room").removeAttr('disabled');
        $('#room_chat_otSessionId').removeAttr('disabled');
        $('#chatbox').html('');
        $('#clients-list').html('');
        $('#welcome').html('');
        $('.chatbox').hide();
        $('#chatbox').hide();

        session.off();
        delete session;
        session = null;

        connectionCount = 0;
    });

    session.on("signal", function (event) {
        console.log("Signal sent from connection " + event.from.id);
        // Process the event.data property, if there is any data.
        putMessageToChatBox(event);
    });

    // Connect to the Session
    session.connect(token, function (error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (!error) {
            //var publisher = OT.initPublisher('publisher', {
            //    insertMode: 'append',
            //    width: '100%',
            //    height: '100%'
            //});
            //
            //session.publish(publisher);

            $('#start_chat').hide();
            $('#stop_chat').show();
            $("#submitmsg").removeAttr('disabled');
            $("#usermsg").removeAttr('disabled');
            $('#start_chat').removeAttr('disabled');
            $('#welcome').html('<b>Common chat</b>');
            addUsersInChatLabel();
            $("#usermsg").focus();

        } else {
            alert('There was an error connecting to the session: ' + error.code + ' ' + error.message);
            $('#start_chat').show();
            $('#stop_chat').hide();
            $("#submitmsg").attr('disabled', 'disabled');
            $("#usermsg").attr('disabled', 'disabled');
            $('#start_chat').attr('disabled', 'disabled');
            $('#room_chat_otSessionId').removeAttr('disabled');
        }

    });

}
