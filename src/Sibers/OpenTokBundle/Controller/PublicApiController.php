<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 13.08.15
 * Time: 15:55
 */

namespace Sibers\OpenTokBundle\Controller;

use Sibers\OpenTokBundle\Entity\RoomChat;
use Sibers\OpenTokBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class PublicApiController
 * @package Sibers\OpenTokBundle\Controller
 * @author Roman Chudov <roman.chudov@gmail.com>
 *
 * @Route("/api/public")
 */
class PublicApiController extends Controller
{

    /**
     * @Route("/get/rooms", name="get_all_rooms_public")
     * @Template()
     */
    public function getAllRoomsAction()
    {
        $rooms = $this->getDoctrine()->getRepository('SibersOpenTokBundle:RoomChat')->findAll();
        $result = array();
        $result['success'] = true;
        $result['count'] = 0;
        $result['api_key'] = $this->container->getParameter('tokbox_api_key');
        $internal_sessions = array();

        if ($rooms) {
            $result['count'] = count($rooms);
            /**
             * @var RoomChat $room
             */
            foreach ($rooms as $room) {
                array_push($internal_sessions, array(
//                        'session_id' => $room->getOtSessionId(),
//                        'token' => ,
                        'internal_session_id' => $room->getId()
                    )
                );
            }
        }

        $result['sessions'] = $internal_sessions;

        return new JsonResponse($result);
    }

    /**
     * @Route("/get/users", name="get_all_users_public")
     * @Template()
     */
    public function getAllUsersAction()
    {
        $users = $this->getDoctrine()->getRepository('SibersOpenTokBundle:User')->findAll();
        $result = array();
        $result['success'] = true;
        $result['count'] = 0;
        $users_result_array = array();

        if ($users) {
            $result['count'] = count($users);
            /**
             * @var User $user
             */
            foreach ($users as $user) {
                array_push($users_result_array, array(
                        'id' => $user->getId(),
                        'username' => $user->getUsername(),
                        'email' => $user->getEmail()
                    )
                );
            }
        }

        $result['users'] = $users_result_array;

        return new JsonResponse($result);
    }
}
