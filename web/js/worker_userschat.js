/**
 * Created by chudov on 17.08.15.
 */

function initializeSession() {

    // Initialize Session Object
    session = OT.initSession(apiKey, sessionId);

    // Handler for connectionCreated event
    session.on("connectionCreated", function (event) {
        if (event.connection.connectionId != session.connection.connectionId) {
            $('.status-indicator').addClass('online');
        }
    });

    // Handler for connectionDestroyed event
    session.on("connectionDestroyed", function (event) {
        $('#welcome').html('');
        if (event.connection.connectionId != session.connection.connectionId) {
            $('.status-indicator').removeClass('online');
        }
    });

    // Handler for sessionDisconnected event
    session.on('sessionDisconnected', function (event) {
        $('#start_chat').show();
        $('#stop_chat').hide();
        $("#submitmsg").attr('disabled', 'disabled');
        $("#usermsg").attr('disabled', 'disabled');
        $('#user_username').removeAttr('disabled');
        $('#chatbox').html('');
        $('.status-indicator').removeClass('online');

        session.off();
        delete session;
        session = null;
    });

    session.on("signal", function (event) {
        console.log("Signal sent from connection " + event.from.id);
        // Process the event.data property, if there is any data.
        putMessageToChatBox(event);
    });

    // Connect to the Session
    session.connect(token, function (error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (!error) {
            //var publisher = OT.initPublisher('publisher', {
            //    insertMode: 'append',
            //    width: '100%',
            //    height: '100%'
            //});
            //
            //session.publish(publisher);

            $('#start_chat').hide();
            $('#stop_chat').show();
            $("#submitmsg").removeAttr('disabled');
            $("#usermsg").removeAttr('disabled');
            $('#start_chat').removeAttr('disabled');
            $("#usermsg").focus();

        } else {
            alert('There was an error connecting to the session: ' + error.code + ' ' + error.message);
            $('#start_chat').show();
            $('#stop_chat').hide();
            $("#submitmsg").attr('disabled', 'disabled');
            $("#usermsg").attr('disabled', 'disabled');
            $('#start_chat').attr('disabled', 'disabled');
            $('#user_username').removeAttr('disabled');
        }

    });

}
