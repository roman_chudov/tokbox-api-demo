/**
 * Created by Roman Chudov on 13.08.15.
 */

$(document).ready(function () {
    $("#usermsg").attr('disabled', 'disabled');
    $('#room_chat_otSessionId').addClass('form-control');

    initNewRoomBtn();
    initStartChatBtn();
    initStopChatBtn();
    initSendBtn();
});


function getNewSessionAndToken() {

    // make an Ajax Request to get the apiKey, sessionId and token from the server
    $.get(SERVER_BASE_URL + '/api/protected/create/room', function (res) {

        if (res.success) {

            var select_elem = $('<option></option>')
                .attr("value", res.internal_session_id)
                .text('Chat Room ' + res.internal_session_id);
            $(select_elem).prependTo($('#room_chat_otSessionId'));
            $('#room_chat_otSessionId').val(res.internal_session_id);
        } else {
            alert('Error: ' + res.message + '(' + res.error_code + ')');
        }
    });

}

function initStartChatBtn() {
    $('#start_chat').click(function () {
        $('#chatbox').html('');
        $('#clients-list').html('');
        $('#start_chat').attr('disabled', 'disabled');
        $("#create_new_room").attr('disabled', 'disabled');
        $('#room_chat_otSessionId').attr('disabled', 'disabled');

        var internal_session_id = $('#room_chat_otSessionId').val();
        if (internal_session_id) {

            $.get(SERVER_BASE_URL + '/api/protected/get/sessionparams/' + internal_session_id, function (res) {

                if (res.success) {

                    apiKey = res.api_key;
                    sessionId = res.session_id;
                    token = res.token;

                    console.log('API Key: ' + apiKey);
                    console.log('Session ID: ' + sessionId);
                    console.log('Token: ' + token);

                    getOldMessages(SERVER_BASE_URL + '/message/get/for/room/' + internal_session_id);

                    initializeSession();
                } else {
                    alert('error');
                    $('#start_chat').removeAttr('disabled');
                    $("#create_new_room").removeAttr('disabled');
                }
            });

        } else {
            alert('Select or create room!');
            $('#start_chat').removeAttr('disabled');
            $("#create_new_room").removeAttr('disabled');
            $('#room_chat_otSessionId').removeAttr('disabled');
        }
    });
}

function initStopChatBtn() {
    $('#stop_chat').hide();

    $('#stop_chat').click(function () {
        if (session) {
            session.disconnect();
        } else {
            alert('Session not created!');
        }
    });
}

function initNewRoomBtn() {
    $('#create_new_room').click(function () {
        getNewSessionAndToken();
    });
}

function initSendBtn() {

    $("#submitmsg").attr('disabled', 'disabled');

    $("#submitmsg").click(function () {
        var clientmsg = $("#usermsg").val().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var room_id = $('#room_chat_otSessionId').val();
        $("#usermsg").val('');

        $.get(SERVER_BASE_URL + '/message/put/room/' + room_id + '/body/' + clientmsg, function (res) {
        });

        var params = new Array;
        if (current_to_connection_id) {
            params = {
                to: session.connections.get(current_to_connection_id),
                data: clientmsg,
                type: 'p2p'
            };

            var msg = $('<div class="chat-message">' + '<b>' + userName + ':</b> ' + clientmsg + '</div>');
            var chatBox = $('#p2p-' + current_to_connection_id);
            $(msg).appendTo(chatBox);
        } else {
            params = {
                data: clientmsg,
                type: 'all'
            };
        }

        session.signal(
            params,
            function (error) {
                if (error) {
                    console.log("signal error ("
                    + error.code
                    + "): " + error.message);
                } else {
                    console.log("signal sent.");
                }
            }
        );
    });
}

function initUserListItem(connection) {

    var label = connection.data;
    if (connection.id == session.connection.id) {
        label = 'Common Chat';
    }

    var item = $('<a></a>')
        .attr('id', connection.id)
        .attr('class', 'list-group-item')
        .attr('href', 'Javascript: void(0);')
        .text(label)
        .appendTo($('#clients-list'));
    ;

    if (connection.id == session.connection.id) {
        item.attr('disabled', 'disabled');
    } else {
        var p2pChatBox = $('<div></div>')
                .attr('id', 'p2p-' + connection.id)
                .attr('class', 'chatbox')
                .appendTo('#chatbox-container')
            ;
        p2pChatBox.hide();
    }

    $('#' + connection.id).click(function () {
        if (connection.id != session.connection.id) {

            var p2pChatId = '#p2p-' + connection.id;
            current_to_connection_id = connection.id;

            if ($(p2pChatId).length) {

                if ($(p2pChatId).css('display') == 'none') {
                    $(p2pChatId).show();
                    $('#chatbox').hide();
                } else {
                    //$(p2pChatId).hide();
                    //$('#chatbox').show();
                }
            }

            $('#welcome').html('<b>' + label + ' chat</b>');
        } else {
            if (current_to_connection_id) {
                $('#p2p-' + current_to_connection_id).hide();
                current_to_connection_id = null;
            }
            $('#chatbox').show();
            $('#welcome').html('<b>Common chat</b>');
        }
    });
}

function addUsersInChatLabel() {
    $('<a class="list-group-item" href="Javascript: void(0);" disabled="disabled"><b>Users in chat:</b></a>').appendTo('#clients-list');
}
