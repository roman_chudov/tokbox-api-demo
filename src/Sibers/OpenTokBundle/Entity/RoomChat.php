<?php

namespace Sibers\OpenTokBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoomChat
 *
 * @ORM\Entity(repositoryClass="Sibers\OpenTokBundle\Entity\RoomChatRepository")
 */
class RoomChat extends Session
{
    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    protected $name;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getType()
    {
        return 'room_chat';
    }

    public function getEntityName()
    {
        return 'RoomChat';
    }

    public function __toString()
    {
        return 'Chat Room ' . $this->getId();
    }
}
