<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 23.09.15
 * Time: 18:43
 */

namespace Sibers\OpenTokBundle\Service\Orm;

use Sibers\OpenTokBundle\Entity\User;
use Sibers\OpenTokBundle\Entity\MobileAccessToken;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MobileAccessTokenOrmManager
{
    protected $em;
    protected $repo;
    protected $parameterBag;

    /**
     * Constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, ParameterBagInterface $parameterBag)
    {
        $this->em = $em;
        $this->repo = $em->getRepository('SibersOpenTokBundle:MobileAccessToken');
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param User $user
     * @return MobileAccessToken
     */
    public function createNewAccessToken(User $user)
    {
        $encoder = new MessageDigestPasswordEncoder();

        $array_for_token = array();
        $array_for_token[] = rand();
        $array_for_token[] = time();
        $array_for_token[] = $user->getUsername();
        $array_for_token[] = $user->getSalt();
        shuffle($array_for_token); // TODO: remove this line if you want :)

        $token = $encoder->encodePassword(
            md5(implode('#', $array_for_token)),
            md5($this->parameterBag->get('secret'))
        );

        $access_token = new MobileAccessToken();
        $access_token->setUser($user);
        $access_token->setAccessToken($token);
        $access_token->setExpiresAt($this->generateExpiresAt());

        $this->em->persist($access_token);
        $this->em->flush();

        return $access_token;
    }

    /**
     * @param User $user
     * @return MobileAccessToken|null
     */
    public function getLastActive(User $user)
    {

        $access_token = $this->repo->getLastActive($user);
        $access_token->setExpiresAt($this->generateExpiresAt());

        $this->em->persist($access_token);
        $this->em->flush();

        return $access_token;
    }

    /**
     * @param User $user
     * @return int
     */
    public function getTokensCreatedTodayCnt(User $user)
    {
        return $this->repo->getTokensCreatedTodayCnt($user);
    }

    /**
     * @return \Sibers\OpenTokBundle\Entity\MobileAccessTokenRepository
     */
    public function getRepository()
    {
        return $this->repo;
    }

    /**
     * @return \DateTime
     */
    protected function generateExpiresAt()
    {
        return new \DateTime(date(
            'M d Y H:i:s',
            time() + intval($this->parameterBag->get('mobile_access_token_lifetime'))
        ));
    }
}