/**
 * Created by chudov on 17.08.15.
 */

// Declare Variables
var apiKey,
    sessionId,
    token,
    session,
    userId,
    userName,
    connectionCount,
    current_to_connection_id
    ;

function getOldMessages(url) {
    $.get(url, function (res) {

        if (res.success) {
            var chatBox = $('#chatbox');

            for (var idx = 0; idx < res.count; ++idx) {
                _putMessageToChatBox(res.messages[idx].from, res.messages[idx].body, chatBox);
            }
        }
    });
}

function _putMessageToChatBox(from, body, chatBox) {

    var msg = $('<div class="chat-message">' + '<b>' + from + ':</b> ' + body + '</div>');
    msg.appendTo(chatBox);
    chatBox.scrollTop(chatBox.prop("scrollHeight"));
}

function putMessageToChatBox(event) {

    var chatBox;

    if (event.type == 'signal:all') { // all

        chatBox = $('#chatbox');

    } else if (event.type == 'signal:p2p') { // me

        var another_id = event.from.id;
        var p2pChatId = '#p2p-' + another_id;
        if ($(p2pChatId).length == 0) {
            initUserListItem(session.connection.get(another_id));
        }

        $('#' + another_id).trigger('click');
        chatBox = $('#p2p-' + another_id);
    }

    _putMessageToChatBox(event.from.data, event.data, chatBox);
    $("#usermsg").focus();
}