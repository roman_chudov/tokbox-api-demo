<?php
/**
 * Created by PhpStorm.
 * User: chudov
 * Date: 20.08.15
 * Time: 14:29
 */

namespace Sibers\OpenTokBundle\Security\Authentication\Provider;

use Doctrine\ORM\EntityManager;
use Sibers\OpenTokBundle\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Util\StringUtils;

use Sibers\OpenTokBundle\Security\Authentication\Token\WsseUserToken;

class WsseProvider implements AuthenticationProviderInterface
{
    private $userProvider;
    private $cacheDir;
    private $entityManager;
    private $parameterBag;

    public function __construct(UserProviderInterface $userProvider,
                                $cacheDir,
                                EntityManager $entityManager,
                                ParameterBagInterface $parameterBag)
    {
        $this->userProvider = $userProvider;
        $this->cacheDir = $cacheDir;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param TokenInterface $token
     * @return WsseUserToken
     */
    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->getUsername());

//        if ($user && $this->validateDigest($token->digest, $token->nonce, $token->created, $user->getPassword())) {
        if ($user && $this->validateAccessToken($user, $token->access_token)) {
            $authenticatedToken = new WsseUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException($this->parameterBag->get('mobile_auth_failed_message'),
            $this->parameterBag->get('mobile_auth_failed_error_code')
        );
    }

    /**
     * @param $user
     * @param $access_token
     * @return bool
     */
    protected function validateAccessToken($user, $access_token)
    {
        $token = $this->entityManager->getRepository('SibersOpenTokBundle:MobileAccessToken')->findOneBy(array(
            'user' => $user->getId(),
            'access_token' => $access_token
        ));

        if ($token) {
            if ($token->getExpiresAt()->getTimestamp() > time()) {
                return true;
            }

            throw new AuthenticationException($this->parameterBag->get('mobile_auth_token_expired_message'),
                $this->parameterBag->get('mobile_auth_token_expired_error_code')
            );
        } else {
            throw new AuthenticationException($this->parameterBag->get('mobile_auth_token_not_found_message'),
                $this->parameterBag->get('mobile_auth_token_not_found_error_code')
            );
        }

        return false;
    }

    /**
     * @param User $user
     * @param $digest
     */
    protected function validateDigestTmp($user, $digest)
    {
        return true;
    }

    /**
     * This function is specific to Wsse authentication and is only used to help this example
     *
     * For more information specific to the logic here, see
     * https://github.com/symfony/symfony-docs/pull/3134#issuecomment-27699129
     */
    protected function validateDigest($digest, $nonce, $created, $secret)
    {
        // Check created time is not in the future
        if (strtotime($created) > time()) {
            return false;
        }

        // Expire timestamp after 5 minutes
        if (time() - strtotime($created) > 300) {
            return false;
        }

        // Validate that the nonce is *not* used in the last 5 minutes
        // if it has, this could be a replay attack
        if (file_exists($this->cacheDir . '/' . $nonce) && file_get_contents($this->cacheDir . '/' . $nonce) + 300 > time()) {
            throw new NonceExpiredException('Previously used nonce detected');
        }
        // If cache directory does not exist we create it
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir, 0777, true);
        }
        file_put_contents($this->cacheDir . '/' . $nonce, time());

        // Validate Secret
        $expected = base64_encode(sha1(base64_decode($nonce) . $created . $secret, true));

        return StringUtils::equals($expected, $digest);
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof WsseUserToken;
    }
}